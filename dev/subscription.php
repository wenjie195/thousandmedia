<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Subscription | Thousand Media</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
<meta property="og:url" content="https://thousandmedia.asia/subscription.php" />
<meta property="og:type" content="website" />
<meta property="og:image" content="https://thousandmedia.asia/img/thousand-media/thousand-media-fb.jpg" />
<meta property="og:title" content="Subscription | Thousand Media" />
<meta property="og:description" content="Thousand Media is a digital marketing agency who helps to increase ROIs, brand awareness and online presence with minimal investment, one business at a time. It is located in Penang, Malaysia." />
<meta name="description" content="Thousand Media is a digital marketing agency who helps to increase ROIs, brand awareness and online presence with minimal investment, one business at a time. It is located in Penang, Malaysia." />
<meta name="author" content="Thousand Media">
<meta name="keywords" content="Thousand Media, ThousandMedia, subscription, 1000 Media, 1000Media, digital marketing, marketing, branding, advertising, social media management, Facebook, Instagram, marketing service provider, online business, cheap, market, SEO, EDM, marketing report, Penang, Malaysia, digital campaign, website, web design, web development, app, app development, video, film, influencer, influencer marketing, blog, article, tips, news, etc">

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-137506603-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-137506603-1');
</script>
<script>
    $(window).load(function(){
       // PAGE IS FULLY LOADED  
       // FADE OUT YOUR OVERLAYING DIV
       $('#overlay').fadeOut();
    });
    </script>
    <script>
    ;(function(){
      function id(v){return document.getElementById(v); }
      function loadbar() {
        var ovrl = id("overlay"),
            prog = id("progress"),
            stat = id("progstat"),
            img = document.images,
            c = 0;
            tot = img.length;
    
        function imgLoaded(){
          c += 1;
          var perc = ((100/tot*c) << 0) +"%";
          prog.style.width = perc;
          stat.innerHTML = "Loading "+ perc;
          if(c===tot) return doneLoading();
        }
        function doneLoading(){
          ovrl.style.opacity = 0;
          setTimeout(function(){ 
            ovrl.style.display = "none";
          }, 1200);
        }
        for(var i=0; i<tot; i++) {
          var tImg     = new Image();
          tImg.onload  = imgLoaded;
          tImg.onerror = imgLoaded;
          tImg.src     = img[i].src;
        }    
      }
      document.addEventListener('DOMContentLoaded', loadbar, false);
    }());
    </script>
   <!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window,document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
 fbq('init', '390782708409897'); 
fbq('track', 'PageView');
</script>
<noscript>
 <img height="1" width="1" 
src="https://www.facebook.com/tr?id=390782708409897&ev=PageView
&noscript=1"/>
</noscript>
<!-- End Facebook Pixel Code -->
  <link rel="canonical" href="https://thousandmedia.asia/subscription.php" />
  <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="css/main.css">
  <link rel="stylesheet" type="text/css" href="css/thousandmedia-style.css">
  <link rel="icon" href="./img/thousand-media/thousand-media-favicon.png"   />
</head>

<body class="body" >


<!-- Load Facebook SDK for JavaScript -->
<div id="fb-root"></div>
<script>
window.fbAsyncInit = function() {
  FB.init({
    xfbml            : true,
    version          : 'v3.2'
  });
};

(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<!-- Your customer chat code -->
<div class="fb-customerchat"
  attribution=install_email
  page_id="2058716717569300"
  theme_color="#fa3c4c"
  logged_in_greeting="Hi! How can we help you?"
  logged_out_greeting="Hi! How can we help you?">
</div>



<?php include 'profile-header.php'; ?>
<div class="width100 same-padding">
		<h3 class="thousand-h3 blog-title margin-top-0">Subscription</h3>
        <div class="gradient-border first-div-gradient blog-gradient"></div>
		<div class="title-content-divider"></div>
        <div class="white-box-shadow width100 overflow">
        	<div class="float-left img-div">
            	<img src="img/thousand-media/basicpack.png" class="plan-img" height="100%">
            </div>
            <div class="float-left content-part">
            	<p class="red-text profile-box-title">Basic Pack</p>
                <table class="box-table">
                	<tr>
                    	<td class="first-td bold-td">Start Date</td>
                        <td class="second-td bold-td">:</td>
                        <td class="third-td">10/05/2019</td>
                    </tr>
                 	<tr>
                    	<td class="first-td bold-td">End Date</td>
                        <td class="second-td bold-td">:</td>
                        <td class="third-td">10/08/2019</td>
                    </tr>                   
                 	<tr>
                    	<td class="first-td bold-td">Fee</td>
                        <td class="second-td bold-td">:</td>
                        <td class="third-td">RM1,800</td>
                    </tr>  
                  	<tr>
                    	<td class="first-td bold-td">Next Payment Date</td>
                        <td class="second-td bold-td">:</td>
                        <td class="third-td">10/06/2019</td>
                    </tr>                    
                                      
                </table>
            </div>
            <div class="float-right pay-div">
            	<a class="hover-opacity"><img src="img/thousand-media/pay.png" class="width100 pay-img"></a>
            </div>
        </div>



    <div class="new-section-divider clear"></div>
    <h3 class="thousand-h3 blog-title mtop-0">Transaction History</h3>
    <div class="gradient-border first-div-gradient blog-gradient"></div>
    <div class="title-content-divider"></div>	
    <div class="white-box-shadow width100 overflow">
                <table class="box-table tran-table">
                	<tr class="transaction-tr1">
                    	<th class="transaction1">Payment Date</th>
                        <th class="transaction2">Plan</th>
                        <th class="transaction3">Receipt No.</th>
                        <th class="transaction4">Payment (RM)</th>                        
                    </tr>                
                
                	<tr>
                    	<td class="transaction1 top-td">24/05/2018</td>
                        <td class="transaction2 top-td">Basic Pack - Setup Fee + Monthly Fee</td>
                        <td class="transaction3 top-td">111920847</td>
                        <td class="transaction4 top-td">1,800</td>                        
                    </tr>
                	<tr>
                    	<td class="transaction1">24/06/2018</td>
                        <td class="transaction2">Basic Pack - Monthly Fee</td>
                        <td class="transaction3">111920847</td>
                        <td class="transaction4">1,800</td>                        
                    </tr>                  
                	<tr>
                    	<td class="transaction1">24/07/2018</td>
                        <td class="transaction2">Basic Pack - Monthly Fee</td>
                        <td class="transaction3">111920847</td>
                        <td class="transaction4">1,800</td>                        
                    </tr> 
                	<tr>
                    	<td class="transaction1">24/08/2018</td>
                        <td class="transaction2">Basic Pack - Monthly Fee</td>
                        <td class="transaction3">111920847</td>
                        <td class="transaction4">1,800</td>                        
                    </tr>                   
                                      
                </table>
    </div>

    <div class="new-section-divider clear"></div>
    <h3 class="thousand-h3 blog-title mtop-0">Current Plan Details - Basic Pack</h3>
    <div class="gradient-border first-div-gradient blog-gradient"></div>
    <div class="title-content-divider"></div>
    <div class="white-box-shadow width100 overflow">
                <table class="box-table tran-table">
                	<tr class="transaction-tr1">
                    	<th class="transaction1">Service Provided</th>
                        <th class="transaction2">Quota</th>                
                    </tr>                
                
                	<tr>
                    	<td class="transaction1 top-td">Marketing strategy</td>
                        <td class="transaction2 top-td">-</td>                      
                    </tr>
                	<tr>
                    	<td class="transaction1">SEO guideline</td>
                        <td class="transaction2">-</td>                     
                    </tr>                  
                	<tr>
                    	<td class="transaction1">Basic branding guideline</td>
                        <td class="transaction2">-</td>                      
                    </tr> 
                	<tr>
                    	<td class="transaction1">Artwork design</td>
                        <td class="transaction2">2/Month</td>                      
                    </tr>                   
                	<tr>
                    	<td class="transaction1">Social media caption enhancement</td>
                        <td class="transaction2">2/Month</td>                      
                    </tr>     
                	<tr>
                    	<td class="transaction1">Social media publishing</td>
                        <td class="transaction2">2/Month</td>                      
                    </tr>                     
                	<tr>
                    	<td class="transaction1">EDM blasting</td>
                        <td class="transaction2">1/Month</td>                      
                    </tr>                    
                	<tr>
                    	<td class="transaction1">Monthly marketing report</td>
                        <td class="transaction2">-</td>                      
                    </tr>       
                	<tr>
                    	<td class="transaction1">Digital campaign planning</td>
                        <td class="transaction2">1/Year</td>                      
                    </tr>                                                                      
                </table>
    </div>    
    <div class="new-section-divider clear"></div>    
    
    
    
    	     
</div>




<div class="footer-div width100 same-padding">
    <p class="footer-p white-text text-center">© 2019 Thousand Media, All Rights Reserved.</p>
</div>
<?php include 'js.php'; ?>
<style>
.subscription-menu{
	color:#d60d26;
	font-weight:600;}
.menu-bg{
	background-color:white !important;
	background:white !important;}
</style>


 

</body>
</html>