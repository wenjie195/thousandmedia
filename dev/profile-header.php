<header id="header" class="header header--fixed same-padding header1 menu-white white-bg" role="banner">
    <div class="big-container-size hidden-padding">
    	<div class="left-logo-div float-left hidden-logo-padding">
    		<a href="index.php" class="hover-opacity"><img src="img/thousand-media/logo.png" class="logo-img" alt="Thousand Media" title="Thousand Media"></a>           
   		</div>
        
        <div class="right-menu-div float-right" id="top-menu">
        	<a href="dashboard.php" class="menu-padding red-hover blog-menu-a red-menu dashboard-menu">Dashboard</a>
            <a href="subscription.php" class="menu-padding red-hover blog-menu-a red-menu subscription-menu">Subscription</a>
            <a href="settings.php" class="menu-padding red-hover blog-menu-a red-menu settings-menu">Settings</a>
            <a href="index.php#contact" class="menu-padding red-hover blog-menu-a red-menu contact-menu">Contact Us</a>
            <a href="index.php" class="menu-padding red-hover blog-menu-a red-menu">Logout</a>
        
		<!-- Mobile View-->
            <a href="dashboard.php" class="white-text menu-padding red-hover2 blog-menu-a2">
            	<img src="img/thousand-media/dashboard.png" class="menu-img" alt="Dashboard" title="Dashboard">
            </a>
            <a href="subscription.php" class="white-text menu-padding red-hover2 blog-menu-a2">
            	<img src="img/thousand-media/subscription.png" class="menu-img" alt="Subscription" title="Subscription">
            </a>
             <a href="settings.php" class="white-text menu-padding red-hover2 blog-menu-a2">
            	<img src="img/thousand-media/settings.png" class="menu-img" alt="Settings" title="Settings">
            </a>           
            
            <a href="index.php#contact" class="white-text menu-padding red-hover2 blog-menu-a2">
            	<img src="img/thousand-media/phone2.png" class="menu-img blog-menu-img" alt="Contact Us" title="Contact Us">            
            </a>
           
            <a href="index.php" class="white-text menu-padding red-hover2 blog-menu-a2">
            	<img src="img/thousand-media/logout3.png" class="menu-img blog-menu-img" alt="Logout" title="Logout">            
            </a>
		<!-- Modal-->               
        </div>
	</div>

</header>